# What effect does varying the Spin Distribution have?

## LIGO Population Inference Paper's Spin Distribution

This was a Beta distribution, and constrained the spin to be between 0 and 1. Peaks at about 0.2, and thus causes a dearth of high spin events.

Produces **24** events which are detectable beyond the FERMI fluence limit. The GW regime is much more forgiving (since the detector sensitivity is assumed to be a *design* sensitivity!), and roughly 12k events have a GW SNR of > 10.

## Uniform Spin Distribution

This was just $\mathcal{U}(0, 1)$. Gives a lot more high spin events, and thus produces **832** events which are detectable beyond the FERMI fluence limit. Roughly 12k again are detectable in the GW regime.**300** of those events are then jointly detectable.

## Gaussian Distributions

These are distributions rooted in the fact that we don't know exactly which black hole spin distributions are more/less probable for NS-BH binaries.

1. $\mu$ = 0.2, $\sigma$ = 0.2
    a. Produces **19**'visible' events.
    b. Roughly similar (~12k) detected in the GW regime as before.

2. $\mu$ = 0.5, $\sigma$ = 0.2
    a. Produces **266** 'visible' events.
    b. Roughly the same number in GW regime as before. SNRs don't change!

3. $\mu = 0.8$, $\sigma$ = 0.2
    a. Produces **882***'visible' events.
    b. Same as before.
